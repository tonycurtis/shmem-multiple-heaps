/**
 * heaps are managed through this type
 */

typedef void *heap_space_t;

heap_space_t space_create(void *addr, size_t);
size_t space_destroy(heap_space_t space);
void *space_malloc(heap_space_t space, size_t size);
void space_free(heap_space_t space, void *addr);
