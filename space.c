#include <sys/types.h>

#include "./space.h"

/**
 * This is the dlmalloc version
 *
 */


#include "./dlmalloc.h"

heap_space_t
space_create(void *addr, size_t size)
{
    return create_mspace_with_base(addr,
                                   size,
                                   1 /* locked */
                                   );
}

size_t
space_destroy(heap_space_t space)
{
    return destroy_mspace(space);
}

void *
space_malloc(heap_space_t space, size_t size)
{
    return mspace_malloc(space, size);
}

void
space_free(heap_space_t space, void *addr)
{
    mspace_free(space, addr);
}

#if 0

/**
 * memkind
 *
 */

#include <memkind.h>

heap_space_t
space_create(void *addr, size_t size)
{
    return k;
}

size_t
space_destroy(heap_space_t space)
{
    return destroy_mspace(space);
}

void *
space_malloc(heap_space_t space, size_t size)
{
    return memkind_malloc(MEMKIND_HBW_PREFERRED, size);
}

void
space_free(heap_space_t space, void *addr)
{
    mspace_free(space, addr);
}
#endif
