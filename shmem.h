void shmem_init(void);
void shmem_finalize(void);

#define BIT_FIELD(N) (1 << (N))

typedef enum heap_cap {
    HEAP_USES_ANY = 0,
    HEAP_USES_DRAM = BIT_FIELD(0),
    HEAP_USES_MCDRAM = BIT_FIELD(1),
} heap_cap_t;

int shmem_heap_create(size_t size, heap_cap_t caps);
int shmem_heap_destroy(int index);
void *shmem_heap_malloc(size_t size, int bindex);

void shmem_free(void *addr);

void shmem_barrier_all(void);
