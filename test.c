#include <stdio.h>
#include <assert.h>

#include "./shmem.h"

int
main()
{
    int h0, h1, h2;
    int *ip1, *ip2;
    long *lp;
    float *fp;
    double *dp;

    shmem_init();

    /* create 3 heaps, with different memory-type hints */
    h0 = shmem_heap_create(1048576, HEAP_USES_ANY);
    h1 = shmem_heap_create(8388608, HEAP_USES_DRAM);
    h2 = shmem_heap_create(16777216, HEAP_USES_MCDRAM);

    shmem_barrier_all();

    /* allocate some variables in these heaps */
    ip1 = (int *) shmem_heap_malloc(sizeof(*ip1) * 128, h0);
    assert(ip1 != NULL);

    ip2 = (int *) shmem_heap_malloc(sizeof(*ip2) * 128, h0);
    assert(ip2 != NULL);

    fp = (float *) shmem_heap_malloc(sizeof(*fp) * 1024, h1);
    assert(fp != NULL);

    lp = (long *) shmem_heap_malloc(sizeof(*lp) * 1024, h2);
    assert(lp != NULL);

    dp = (double *) shmem_heap_malloc(sizeof(*dp) * 2, h2);
    assert(dp != NULL);

    /* fill in some values */
    ip1[94] = 12345;
    fp[999] = 3.141592654;
    lp[44] = 987654321;
    dp[0] = 1.2345;

    shmem_barrier_all();

    /* make sure stored values match */
    printf("ip1[94] = %d\n",   ip1[94]);
    printf("ip2[33] = %d\n",   ip2[33]);
    printf("fp[999] = %.4f\n", fp[999]);
    printf("lp[44]  = %ld\n",  lp[44]);
    printf("dp[0]   = %.4f\n", dp[0]);

    shmem_barrier_all();

    /* clean up and exit */
    shmem_free(dp);
    shmem_free(lp);
    shmem_free(fp);
    shmem_free(ip2);
    shmem_free(ip1);

    (void) shmem_heap_destroy(h2);
    (void) shmem_heap_destroy(h1);
    (void) shmem_heap_destroy(h0);

    shmem_finalize();

    return 0;
}
