CC = gcc
CFLAGS = -ggdb -Wall -pedantic

LD = $(CC)
LDFLAGS = -ggdb

CPPCHECK_OPTIONS = --enable=all
SPLINT_OPTIONS = -strict

SOURCES = shmem.c space.c test.c
OBJECTS = $(SOURCES:.c=.o)

OBJECTS += dlmalloc.o

.PHONY: all clean cppcheck splint valgrind

all:	./test

./test:	$(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

DLMALLOC_CFLAGS = \
	-DONLY_MSPACES=1 \
	-DHAVE_MORECORE=0 \
	-DUSE_LOCKS=1

dlmalloc.o:	dlmalloc.c
	$(CC) $(DLMALLOC_CFLAGS) -c -o $@ $^

clean:
	rm -f $(OBJECTS) ./test

#
# note no check of dlmalloc as it is 3rd party
#
cppcheck:	$(SOURCES)
	- $@ $(CPPCHECK_OPTIONS) $^
splint:		$(SOURCES)
	- $@ $(SPLINT_OPTIONS) $^

valgrind:	./test
	- $@ $^

#
#
#
dlmalloc.o: dlmalloc.c
shmem.o: shmem.c shmem.h space.h dlmalloc.h
space.o: space.c space.h dlmalloc.h
test.o: test.c shmem.h

