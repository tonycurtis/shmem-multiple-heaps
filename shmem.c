#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#include "./shmem.h"
#include "./space.h"

typedef struct heap_info {
    void *base;                 /* start of heap */
    size_t size;                /* size of heap */
    heap_space_t space;         /* memory space backing heap */
} heap_info_t;

static heap_info_t *heaps = NULL;
static int heap_max = 0;

/*
 * some play values...
 *
 * this would default to 1 heap for compatibility.  Allow environment
 * variables or other mechanisms to specify how many heaps, their
 * sizes and potential required capabilities at init.  Allowing new
 * heaps during execution requires much more work.
 *
 */

typedef struct heap_desc {
    size_t size;                /* size requested */
    heap_cap_t caps;            /* hints/advice to allocator */
} heap_desc_t;

#if 0
/**
 * human-readable capabilities
 *
 */

static
void
print_heap_caps(heap_cap_t c)
{
    int i;

    for (i = 0; i < ncaps; i += 1) {
        if (BIT_FIELD(i) & c) {
            fprintf(stderr, "%d ", BIT_FIELD(i));
        }
    }
    fprintf(stderr, "\n");

}
#endif

/**
 * create the specified heap of given size and return non-negative
 * handle, or -1 on failure
 *
 */

static
int
heap_init(size_t size, heap_cap_t caps)
{
    int s;
    heap_info_t *heap_ext;
    int heap_curr = heap_max;

    /* count another heap, resize heap index space */
    heap_max += 1;

    heap_ext = realloc(heaps, sizeof(*heaps) * heap_max);
    if (heap_ext == NULL) {
        goto fail;
    }
    heaps = heap_ext;

    /*
     * align to standard page size, there are "correct" ways of
     * getting the 4096 value.
     */
    s = posix_memalign(& heaps[heap_curr].base, 4096, size);
    if (s != 0) {
        goto fail;
    }
    if (heaps[heap_curr].base == NULL) {
        goto fail;
    }

    /* in real life, this might not be exactly what was requested */
    heaps[heap_curr].size = size;

    heaps[heap_curr].space = space_create(heaps[heap_curr].base,
                                          heaps[heap_curr].size
                                          );
    if (heaps[heap_curr].space == NULL) {
        goto fail;
    }

    fprintf(stderr,
            "shmem_init: heap #%-3d size %16lu bytes @ %p\n",
            heap_curr,
            (unsigned long) heaps[heap_curr].size,
            heaps[heap_curr].base
            );
    /* print_heap_caps(heapinfo[heap_curr].caps); */

    return heap_curr;

 fail:
    return -1;
}

/**
 * free up space used for heap management
 *
 */

static
void
heaps_fini(void)
{
    int i;

    /* foreach heap ... */
    for (i = 0; i < heap_max; i += 1) {
        /* free the space */
        const size_t b = space_destroy(heaps[i].space);

        fprintf(stderr,
                "heaps_fini: free %lu bytes in space %d\n",
                (unsigned long) b, i);

        free(heaps[i].base);
    }

    /* free heap index */
    free(heaps);
}

/**
 * work out which heap "addr" is in.  Return index, or -1 on failure
 *
 * so we could try clever address tracking hash tables or similar, but
 * it might be reasonable to assume the number of heaps in use will be
 * small, maybe 1..3, so a linear scan probably is OK here.
 *
 */

static
int
which_heap(void *addr)
{
    const size_t s_addr = (size_t) addr;
    int i;

    for (i = 0; i < heap_max; i += 1) {
        const size_t top = (size_t) heaps[i].base + heaps[i].size;

        if ( (((size_t) heaps[i].base) < s_addr) && (s_addr < top) ) {
            return i;
            /* eureka */
        }
    }
    return -1;
}

/**
 * API
 *
 */

static bool finalized = false;

void
shmem_finalize(void)
{
    if (! finalized) {
        heaps_fini();

        finalized = true;
    }
}

void
shmem_init(void)
{
    (void) atexit(shmem_finalize);
}

/**
 * create new heap of size "size" bytes, provide "caps" as requests
 * for memory types, other capabiltiies.  return index as handle.
 */

int
shmem_heap_create(size_t size, heap_cap_t caps)
{
    return heap_init(size, caps);
}

/**
 * destroy heap with handle "index".  return 0 if successful, otherwise -1
 */

int
shmem_heap_destroy(int index)
{
    /* TODO */
    return 0;
}

/**
 * create memory of size "size" in heap with handle "index".
 * return pointer to memory, or NULL on error
 *
 */

void *
shmem_heap_malloc(size_t size, int index)
{
    void *addr;

    if (index < 0) {
        return NULL;
    }

    addr = space_malloc(heaps[index].space, size);

    fprintf(stderr,
            "shmem_malloc: from heap %d, size %lu bytes @ %p\n",
            index,
            (unsigned long) size,
            addr
            );

    return addr;
}

/**
 * release memory previously allocated at "addr"
 */

void
shmem_free(void *addr)
{
    /* implementation has to look up which heap "addr" is in */
    const int index = which_heap(addr);
    assert(index >= 0);

    fprintf(stderr,
            "shmem_free: pointer %p in heap %d\n",
            addr,
            index
            );

    space_free(heaps[index].space, addr);
}

void
shmem_barrier_all(void)
{
    /* stub */
}
